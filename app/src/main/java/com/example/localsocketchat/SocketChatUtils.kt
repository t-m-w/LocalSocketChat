package com.example.localsocketchat

import android.net.LocalServerSocket
import android.net.LocalSocket
import android.net.LocalSocketAddress
import android.util.Log
import kotlinx.coroutines.*
import java.io.IOException
import java.util.concurrent.atomic.AtomicInteger

class SocketChatUtils(myName: String? = null, peerName: String? = null) {
    var myName : String
    var peerName : String
    private val heldSockets = HashMap<String, HeldSocket>()
    private val heldValuesForPeer = HashMap<String, Byte>()
    private val readyState = HashMap<String, Boolean>()

    init {
        Log.e(TAG, "Initializing new instance of SocketChatUtils")
        var namesNotSupplied = false
        if (myName == null && peerName == null) {
            namesNotSupplied = true
        }
        this.myName = myName ?: DEFAULT_NAME
        this.peerName = peerName ?: ALTERNATE_NAME

        try {
            holdForWorld(COMM_PRESENT)
        }
        catch (e : Exception) {
            if (namesNotSupplied) {
                this.myName = ALTERNATE_NAME
                this.peerName = DEFAULT_NAME
                holdForWorld(COMM_PRESENT)
            } else {
                throw e
            }
        }
        assert(socketNameIsHeld(constructHoldingName(COMM_PRESENT, this.myName, WORLD)))
    }

    companion object {
        const val DEFAULT_NAME = "Alice"
        const val ALTERNATE_NAME = "Bob"
        const val WORLD = "World"
        const val DEBUG = false

        // Indicate availability
        const val COMM_PRESENT = "Present"

        // Toggle between readiness states
        // After reading one value, flip these states
        const val COMM_READYBIT = "Ready"

        private const val COMM_SEPARATOR = ";"

        // Has data to transmit
        const val COMM_HASDATA = "HasData"

        const val SLEEP_MILLIS = 10L

        const val TAG = "SocketChatUtils"

        private fun constructHoldingName(
            what : String,
            byWhom : String,
            forWhom : String,
        ) : String {
            return listOf(byWhom, forWhom, what).joinToString(COMM_SEPARATOR)
        }

        private fun socketNameIsHeld(name: String) : Boolean {
            var socket: LocalSocket? = null
            return try {
                socket = LocalSocket()
                socket.connect(LocalSocketAddress(name))
                true
            } catch (e : IOException) {
                (e.message?.contains("Permission denied") == true)
            } finally {
                socket?.close()
            }
        }
    }

    class HeldSocket(val name : String) {
        private var socket : LocalServerSocket? = LocalServerSocket(name)
        private var listenerCoroutine = CoroutineScope(Dispatchers.Default).launch {
            try {
                if (DEBUG) Log.e(TAG, "accepting for $name...")
                if (socket != null) {
                    socket?.accept()
                } else {
                    Log.e(TAG, "socket was null somehow for $name")
                }
            } catch (e : Exception) {
                Log.e(TAG, "catch: $name", e)
            } finally {
                if (DEBUG) Log.e(TAG, "finally: $name")
            }
        }

        init {
            listenerCoroutine.start()
            while (!socketNameIsHeld(name) && socket != null && listenerCoroutine.isActive) {
                Log.e(TAG, "socket still not held for $name")
                Thread.sleep(SLEEP_MILLIS)
            }
        }

        val held : Boolean
            get() = socket != null

        fun close() {
            if (socket != null) {
                if (DEBUG) Log.e(TAG, "closing socket for $name")
                socket!!.close()
                if (DEBUG) Log.e(TAG, "closed socket for $name (supposedly)")
                socket = null
                listenerCoroutine.cancel()
                if (DEBUG) Log.e(TAG, "cancelled coroutine for $name (supposedly)")
            } else {
                Log.e(TAG, "socket already closed for $name")
            }
        }
    }

    private fun holdBySocketName(socketName : String) {
        if (DEBUG) {
            Log.e(TAG, "holdSocketByName ${socketName}")
        }
        try {
            heldSockets[socketName] = HeldSocket(socketName)
        } catch (e : IOException) {
            if (e.message?.contains("Address already in use") == true) {
                Log.e(TAG, "what the heck", e)
                throw e
            }
        }
    }
    private fun hold(what : String, forWhom : String? = null) {
        val name = constructHoldingName(what, myName, forWhom ?: peerName)
        holdBySocketName(name)
    }
    private fun holdForWorld(what : String) {
        hold(what, WORLD)
    }
    private fun releaseBySocketName(socketName : String) {
        if (DEBUG) {
            Log.e(TAG, "releaseSocketByName $socketName")
        }
        val socket = heldSockets[socketName]
        heldSockets.remove(socketName)
        socket?.close()
    }
    private fun release(what : String, forWhom : String? = null) {
        val name = constructHoldingName(what, myName, forWhom ?: peerName)
        releaseBySocketName(name)
    }
    private fun releaseForWorld(what : String) {
        release(what, WORLD)
    }
    private fun isHeldByMe(what : String, forWhom : String? = null) : Boolean {
        val name = constructHoldingName(what, myName, forWhom ?: peerName)
        if (DEBUG) {
            Log.e(TAG, "isHeldByMe $name")
        }
        return (name in heldSockets)
    }
    private fun isHeldAtAll(what : String, byWhom : String? = null, forWhom : String? = null) : Boolean {
        val name = constructHoldingName(what, byWhom ?: myName, forWhom ?: peerName)
        if (DEBUG) {
            Log.e(TAG, "isHeld $name")
        }
        if (name in heldSockets) {
            return true
        }
        return socketNameIsHeld(name)
    }
    private fun isHeldForMeByPeer(what : String, peer : String? = null) : Boolean {
        val name = constructHoldingName(what, peer ?: peerName, myName)
        if (DEBUG) {
            Log.e(TAG, "isHeld $name")
        }
        if (name in heldSockets) {
            return true
        }
        return socketNameIsHeld(name)
    }
    private fun setValueByBits(what : Byte, forWhom : String? = null) {
        val whom : String = forWhom ?: peerName
        Log.e(TAG, "setValueByBits in $myName to $what for $whom")
        val whatInt : Int = what.toUByte().toInt()
        for (b in 0..Byte.SIZE_BITS) {
            var name = constructHoldingName(b.toString(), myName, whom)
            if ((whatInt and (1 shl b)) != 0) {
                if (name !in heldSockets) {
                    if (DEBUG) Log.e(TAG, "Setting $name...")
                    holdBySocketName(name)
                }
            }
            else {
                if (name in heldSockets) {
                    if (DEBUG) Log.e(TAG, "Releasing $name...")
                    releaseBySocketName(name)
                }
            }
        }
    }
    private fun setValue(what : Byte, forWhom : String? = null) {
        setValueByBits(what, forWhom)
        flipReadyBit(forWhom)
    }

    private suspend fun getValueByBits(byWhom : String? = null) : Byte =
        withContext(Dispatchers.IO) {
            val whom = byWhom ?: peerName
            if (DEBUG) {
                Log.e(TAG, "getValueByBits in $myName for $whom")
                Thread.sleep(500)
            }
            val jobs : MutableList<Job> = mutableListOf()
            val i = AtomicInteger()
            for (b in 0..Byte.SIZE_BITS) {
                jobs.add(launch {
                    val name = constructHoldingName(b.toString(), whom, myName)
                    if (socketNameIsHeld(name)) {
                        i.addAndGet(1 shl b)
                    }
                })
            }
            jobs.joinAll()
            Log.e(TAG, "getValueByBits in $myName returns $i for $whom")
            return@withContext i.toByte()
    }

    // deprecated
    suspend fun checkAllPossibleValuesOneByOne(byWhom : String? = null) : Byte? =
            withContext(Dispatchers.IO) {
        val whom = byWhom ?: peerName
        var foundValue : UByte? = null
        val jobs : MutableList<Job> = mutableListOf()
        for (b in UByte.MIN_VALUE..UByte.MAX_VALUE) {
            jobs.add(launch {
                val name = constructHoldingName(b.toString(), whom, myName)
                if (socketNameIsHeld(name)) {
                    foundValue = b.toUByte()
                }
            })
        }
        jobs.joinAll()
        return@withContext foundValue?.toByte()
    }
    private fun flipReadyBit(forWhom : String? = null) {
        val whom = forWhom ?: peerName
        if (readyState[whom] == true) {
            release(COMM_READYBIT, whom)
            readyState[whom] = false
        } else {
            hold(COMM_READYBIT, whom)
            readyState[whom] = true
        }
    }
    fun isPeerPresent(peer : String? = null) : Boolean {
        val whom = peer ?: peerName
        return socketNameIsHeld(constructHoldingName(COMM_PRESENT, whom, WORLD))
    }
    private suspend fun getValue(whose : String? = null) : Byte {
        val value = getValueByBits(whose)
        flipReadyBit(whose)
        return value
    }
    fun getPeerReadyBit(peer : String? = null) : Boolean {
        return isHeldForMeByPeer(COMM_READYBIT, peer = peer)
    }
    fun writeData(bytes : ByteArray, peer : String? = null, waitForever : Boolean = false) {
        val whom = peer ?: peerName
        if (DEBUG || true) {
            Log.e(TAG, "writeData: $myName: START for $whom")
            Thread.sleep(500)
        }
        var peerReadyBit : Boolean? = null
        var peerLastReadyBit : Boolean? = null
        var claimingToHaveData = false
        try {
            val totalBytes = bytes.size
            // use indexes and include an extra +1 to wait for state sync after last byte
            // (the extra +1 is from the total)
            for (i in 0..totalBytes) {
                if (!isPeerPresent(whom)) {
                    if (peerReadyBit == null && peerLastReadyBit == null) {
                        if (!waitForever) {
                            throw IOException("Peer not present")
                        }
                    }
                    if (!waitForever) {
                        return
                    }
                }
                Thread.sleep(SLEEP_MILLIS)
                while (true) {
                    peerReadyBit = getPeerReadyBit(whom)
                    if (peerReadyBit != peerLastReadyBit) {
                        break
                    } else {
                        Thread.sleep(SLEEP_MILLIS)
                    }
                    if (DEBUG) Log.e(
                        TAG,
                        "writeData: $myName: $whom is not ready yet ($peerReadyBit)..."
                    )
                }
                if (DEBUG) Log.e(
                    TAG,
                    "writeData: $myName: $whom is ready for now ($peerReadyBit)..."
                )
                peerLastReadyBit = peerReadyBit
                if (i >= totalBytes) {
                    break
                }
                setValueByBits(bytes[i])
                Log.e(TAG, "writeData: $myName: wrote ${bytes[i]}")
                Log.e(TAG, "writeData: $myName: Expecting $whom's next ready bit for me to be "
                        + (peerReadyBit != true))
                flipReadyBit(whom)
                if (DEBUG || true) Log.e(TAG,
                    "writeData: $myName: My ready bit for $whom is now ${readyState[whom]}")
                if (!claimingToHaveData) {
                    claimingToHaveData = true
                    hold(COMM_HASDATA, whom)
                }
            }
        } finally {
            if (isHeldByMe(COMM_HASDATA, whom)) {
                release(COMM_HASDATA, whom)
            }
            flipReadyBit(whom)
        }
        Log.e(TAG, "writeData: $myName: END for $whom")
    }
    suspend fun readAllData(peer : String? = null, waitForever : Boolean = false) : ByteArray {
        val whom = peer ?: peerName
        if (DEBUG || true) {
            Log.e(TAG, "readAllData: $myName: START for $whom")
            Thread.sleep(500)
        }
        val bytes : MutableList<Byte> = mutableListOf()
        var peerReadyBit : Boolean? = null
        var peerLastReadyBit : Boolean? = null
        while (true) {
            if (!isHeldForMeByPeer(COMM_HASDATA, peer = whom) || !isPeerPresent(whom)) {
                if (peerReadyBit == null && peerLastReadyBit == null) {
                    if (!waitForever) {
                        // TODO: throw exception or something?
                        Log.e(TAG, "readAllData: $myName: nothing for $whom")
                        return ByteArray(0)
                    }
                } else {
                    return bytes.toByteArray()
                }
                Thread.sleep(SLEEP_MILLIS)
                continue
            }
            peerReadyBit = getPeerReadyBit(whom)
            if (peerReadyBit == peerLastReadyBit) {
                Thread.sleep(SLEEP_MILLIS)
                continue
            }
            if (DEBUG || true) Log.e(TAG,
                "readAllData: $myName: Proceeding now that $whom's ready bit is $peerReadyBit")
            peerLastReadyBit = peerReadyBit
            bytes.add(getValueByBits(whom))
            flipReadyBit(whom)
            if (DEBUG || true) Log.e(TAG,
                "readAllData: $myName: My ready bit for $whom is now ${readyState[whom]}")
            Log.e(TAG, "readAllData: $myName: Expecting $whom's next ready bit for me to be " +
                    (peerLastReadyBit != true))
        }
    }
}