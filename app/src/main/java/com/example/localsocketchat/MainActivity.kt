package com.example.localsocketchat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private var socketChatUtils = SocketChatUtils()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val outputText = findViewById<TextView>(R.id.outputText)

        /*var socketChatUtils2 = SocketChatUtils()
        CoroutineScope(Dispatchers.IO).launch {
            Thread.sleep(1000)
            val output = socketChatUtils2.readAllData(waitForever = true).toString(Charsets.UTF_8)
            CoroutineScope(Dispatchers.Main).launch {
                outputText.apply {
                    text = output.toString()
                }
            }
        }*/

        var output = StringBuilder("Hello.\n")
        if (socketChatUtils.isPeerPresent()) {
            CoroutineScope(Dispatchers.IO).launch {
                val msg = "Hi, it's me, ${socketChatUtils.myName}, and my data dir is " +
                        applicationInfo.dataDir
                socketChatUtils.writeData(msg.toByteArray(Charsets.UTF_8), waitForever = true)
            }
        }

        CoroutineScope(Dispatchers.IO).launch {
            output.appendLine("My name is ${socketChatUtils.myName}.\n" +
                    "I'm trying to chat with ${socketChatUtils.peerName}.\n" +
                    "Open this in another user or profile,\n" +
                    "and then give ${socketChatUtils.peerName} a sec...\n")
            CoroutineScope(Dispatchers.Main).launch {
                outputText.apply {
                    text = output.toString()
                }
            }.join()
            output.appendLine(
                socketChatUtils.readAllData(waitForever = true).toString(Charsets.UTF_8))
            CoroutineScope(Dispatchers.Main).launch {
                outputText.apply {
                    text = output.toString()
                }
            }.join()
            CoroutineScope(Dispatchers.IO).launch {
                val msg = "Oh hey, it's me, ${socketChatUtils.myName}, and my data dir is " +
                        applicationInfo.dataDir
                socketChatUtils.writeData(msg.toByteArray(Charsets.UTF_8), waitForever = true)
            }
        }
    }
}